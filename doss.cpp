/* This file is a part of D.O.S.S (Distributed Object Synchronization System).
 Copyright © 2022 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <iostream>
#include <memory>
#include <list>
#include <map>
#include <string>
#include <stdexcept>
#include <cstdio>
#include <time.h>
#include <io.h>
#include <zlib.h>

using namespace std;


class Creds
{
public:
    typedef shared_ptr<Creds> Ref;

    static Ref Create(string src) {
        return Ref(new Creds(), Creds::Free);
    };

private:
    Creds(){
    };
    virtual ~Creds(){
    };
    static void Free(Creds* lpt) {
        delete lpt;
    };
};


class Resource
{
public:
    typedef shared_ptr<Resource> Ref;
    /// Оперативные режимы ресурса
    enum MODE {
        M_TMP,      /// Ресурс с авто-удалением (отменяется в Move), не должен существовать.
        M_READ,     /// Ресурс для чтения, должен существовать.
        M_WRITE,    /// Ресурс для записи, заменяет или создаёт новый ресурс.
        M_OPERATE,  /// Ресурс в режиме манипуляции (перемещение/удаление), авто-режим по (Move/Remove).
        M_ERROR =-1 /// Ресурс не доступен для манипуляций
    };

    virtual int Mode() {
        return m_mode;
    };

    virtual int Read(void* buf, int cb) {
        return -1;
    };

    virtual int Write(const void* buf, int cb) {
        return -1;
    };

    virtual bool Lock() {
        return false;
    };

    virtual bool Move(string uri) {
        return false;
    };

    virtual bool Remove() {
        return false;
    };

    virtual bool Reset() {
        return false;
    };

    virtual bool Unlock() {
        return false;
    };

    virtual bool Flush() {
        return false;
    };

protected:
    MODE m_mode;
    Resource() {
        m_mode = M_ERROR;
    };
    virtual ~Resource() {
    };
    static void Free(Resource* lpt) {
        if (lpt->m_mode == M_TMP)
            lpt->Remove();
        delete lpt;
    };
};


class GZResource: public Resource
{
public:
    /// Состояние обработки сжатых данных
    enum STATE {
        S_NO,           /// Не инициализирован (до начала обработки)
        S_DEFLATE,      /// Начата распаковка данных
        S_INFLATE,      /// Начата упаковка данных
        S_END,          /// Операция распаковки/упаковки завершена успешно
        S_ERROR = -1,   /// Произошла ошибка при обработке данных
    };

    /// Создать новый gzip-прокси для указанного ресурса
    static Ref Create(Ref target) {
        if (!target || target->Mode() == M_ERROR)
            return Ref(nullptr);
        return Ref(new GZResource(target), Resource::Free);
    };

    virtual int Mode() override {
        return m_ref->Mode();
    };

    /// Текущее состояние обработки
    virtual STATE State() {
        return m_state;
    };

    /// Прокси-функция для чтения сжатого потока
    /// возвращает:
    /// * колличество распакованных байт записанных в буфер
    /// * -1 при неправильной инициализации/параметрах
    /// * -2 при ошибке обработки сжатых данных
    /// * -3 при ошибке чтения сжатых данных
    virtual int Read(void* buf, int cb) override {
        if (!buf || cb <= 0)
            return -1;
        // проверка на доступность чтения + инициализация при необходимости
        if (m_ref->Mode() != M_READ)
            return -1;
        // если обработка чтения (M_READ) завершена
        if (m_state == S_END)
            return 0; // сигнализируем об окончании данных
        if (m_state == S_NO) {
            if (inflateInit2(&m_st, 32+MAX_WBITS) != Z_OK) {
                m_state = S_ERROR;
                return -1;
            }
            m_state = S_INFLATE;
        }
        if (m_state != S_INFLATE)
            return -1;
        // начинаем распаковку
        m_st.next_out = (Bytef*) buf;
        m_st.avail_out = cb;
        // пока оба буфера имеют место для записи...
        while (m_st.avail_out > 0) {
            // если буффер сжатых данных пуст - заполняем
            if (m_st.avail_in == 0) {
                int br = m_ref->Read(m_buf, sizeof(m_buf));
                if (br <= 0) { // прочитать сжатые данные невозможно
                    // если распаковка была удачной И поток полностью считан
                    if (inflateEnd(&m_st) == Z_OK && br == 0) {
                        m_state = S_END;
                        return cb - m_st.avail_out;
                    }
                    m_state = S_ERROR;
                    return -3;
                }
                m_st.next_in = m_buf;
                m_st.avail_in = br;
            }
            int rc = inflate(&m_st, Z_NO_FLUSH);
            if (rc != Z_OK) {
                int erc = inflateEnd(&m_st);
                // если поток распакован и удачно завершен
                if (rc == Z_STREAM_END && erc == Z_OK) {
                    // ресрурс более не содержит данных?
                    if (m_st.avail_in != 0 || m_ref->Read(m_buf, 1) != 0) {
                        cerr << "DEBUG: non consistent zstream! left bytes: "
                             << m_st.avail_in << endl;
                    }
                    m_state = S_END;
                    return cb - m_st.avail_out;
                }
                return -2;
            }
        }
        return cb - m_st.avail_out;
    };

    /// Прокси-функция для записи в сжатый поток
    /// возвращает:
    /// * колличество обработанных байт (при успехе всегда равно cb)
    /// * -1 при неправильной инициализации/параметрах
    /// * -2 при ошибке обработки сжатых данных
    /// * -3 при ошибке записи сжатых данных
    virtual int Write(const void* buf, int cb) override {
        if (!buf || cb < 0)
            return -1;
        // проверка на доступность записи + инициализация при необходимости
        int mode = m_ref->Mode();
        if (mode != M_WRITE && mode != M_TMP)
            return -1;
        if (m_state == S_NO) {
            if (deflateInit(&m_st, Z_DEFAULT_COMPRESSION) != Z_OK) {
                m_state = S_ERROR;
                return -1;
            }
            m_state = S_DEFLATE;
        }
        if (m_state != S_DEFLATE)
            return -1;
        m_st.next_in = (Bytef*) buf;
        m_st.avail_in = cb;
        int rc;
        do { // пока есть данные - сжимаем поблочно
            m_st.next_out = m_buf;
            m_st.avail_out = sizeof(m_buf);
            rc = deflate(&m_st, Z_NO_FLUSH);
            if (rc != Z_OK)
                break;
            // и записываем сжатый результат
            int bw = sizeof(m_buf) - m_st.avail_out;
            if (bw > 0 && m_ref->Write(m_buf, bw) != bw) {
                rc = Z_STREAM_ERROR;
                break;
            }
        } while (m_st.avail_in > 0);
        // проверка успешного сжатия всех переданных данных
        if (rc != Z_OK || m_st.avail_in != 0) {
            m_state = S_ERROR;
            deflateEnd(&m_st);
            return -3;
        }
        return cb; // возвращаем то, сколько обработали а не записали
    };

    /// При использовании функции Write() вызов Flush() крайне желателен, т.к.
    /// если произойдёт ошибка записи буферов в деструкторе - вы об этом уже не
    /// узнаете...
    /// Кроме того, для режима записи состояние S_END наступает только после
    /// вызово Flush()
    virtual bool Flush() override {
        if (m_state != S_DEFLATE)
            return false;
        m_state = S_ERROR;
        int rc;
        do {
            m_st.next_out = m_buf;
            m_st.avail_out = sizeof(m_buf);
            rc = deflate(&m_st, Z_FINISH);
            if (rc != Z_OK && rc != Z_STREAM_END)
                break;
            int cb = sizeof(m_buf) - m_st.avail_out;
            if (cb <= 0)
                break; // нечего записывать
            if (m_ref->Write(m_buf, cb) != cb) {
                rc = Z_STREAM_ERROR;
                break;
            }
        } while (rc == Z_OK);

        deflateEnd(&m_st);
        if (rc != Z_STREAM_END)
            return false;
        m_state = S_END;
        return m_ref->Flush();
    };

    virtual bool Lock() override {
        return m_ref->Lock();
    };

    virtual bool Move(string uri) override {
        if (m_state != S_END)
            return false;
        return m_ref->Move(uri);
    };

    virtual bool Remove() override {
        if (m_state != S_END)
            return false;
         return m_ref->Remove();
    };

    virtual bool Reset() override {
        ZReset();
        return m_ref->Reset();
    };

    virtual bool Unlock() override {
        return m_ref->Unlock();
    };

private:
    Ref m_ref;
    z_stream m_st;
    STATE m_state;
    Bytef m_buf[1024*4];

    void ZReset() {
        if (m_state == S_INFLATE)
            inflateEnd(&m_st);
        else if (m_state == S_DEFLATE)
            deflateEnd(&m_st);
        m_state = S_NO;
        m_st.next_out = (Bytef*) nullptr;
        m_st.avail_out = 0;
        m_st.total_out = 0;
        m_st.next_in = (Bytef*) nullptr;
        m_st.avail_in = 0;
        m_st.total_in = 0;
        m_st.zalloc = Z_NULL;
        m_st.zfree = Z_NULL;
    };

    GZResource(Ref res): m_ref(res) {
        m_state = S_NO;
        ZReset();
    };
    virtual ~GZResource() {
        Flush();
    };
};




class SplitString
{
protected:
    /// Поведение при обнаружении пустых полей
    enum BHV {
        RESTRICT,   /// Полный запрет, список не конструируется
        SCIP,       /// Пропуск, список содержит только не пустые поля
        ALLOW       /// Разрешение, список может содержать пустые поля
    };

    /// Вызывается на каждый элемент в s, индекс: n, начало: b, размер: cc.
    virtual bool Column(const string& s, size_t n, size_t b, size_t cc) = 0;

public:
    /// Поиск и обработка подстрок по разделителю
    virtual size_t Split(const string& src, const string& sep, BHV behave) {
        const size_t sol = src.size();
        if (!sol)
            return string::npos;
        const size_t spl = sep.size();
        if (!spl)
            return string::npos;
        size_t cp = 0, fp = 0, cnt = 0;
        for (;(fp = src.find_first_of(sep, cp)) != string::npos; cp = fp+spl) {
            if (fp == cp) {
                switch (behave) {
                    case BHV::ALLOW: break;
                    case BHV::SCIP:  break;
                    default: return string::npos;
                }
            }
            if (!Column(src, cnt, cp, fp-cp))
                return ++cnt;
            ++cnt;
        }
        if (cp < sol) {
            Column(src, cnt, cp, sol-cp);
            ++cnt;
        }
        return cnt;
    };
};


class JoinTo
{
public:
    /// string rv = JoinTo::Str(list);
    static string Str(shared_ptr< list<string> > stor, string sep) {
        string rv;
        if (!stor)
            return rv;
        bool f = true;
        for (const auto& it : *stor) {
            if (f) f = false;
            else rv += sep;
            rv += it;
        }
        return rv;
    };
};


typedef shared_ptr< list<string> > ListPtr;


class SplitTo: protected SplitString
{
public:
    /// auto lst = SplitTo::List("test:ok:1:2:3:", ":", SplitString::RESTRICT);
    static ListPtr List(const string& st, const string& sp, BHV bv) {
        SplitTo opc;
        if (opc.Split(st, sp, bv) != string::npos)
            return opc.m_stor;
        return ListPtr(nullptr);
    };

protected:
    ListPtr m_stor;

    virtual size_t Split(const string& st, const string& sp, BHV bv) override {
        m_stor = ListPtr(new list<string>());
        return SplitString::Split(st, sp, bv);
    };

    virtual bool Column(const string& s, size_t n, size_t b, size_t cc) {
        m_stor->push_back(cc? s.substr(b, cc) : string());
        return true;
    };
};


class Hash
{
public:
    typedef shared_ptr<Hash> Ref;

    virtual bool Start() = 0;
    virtual bool Next(const void* ptr, int cch) = 0;
    virtual string Digest() = 0;

protected:
    Hash() {
    };
    virtual ~Hash(){
    };
    static void Free(Hash* lpt) {
        delete lpt;
    };
};


class SimpleHash: public Hash
{
public:
    static Ref Create() {
        return Ref(new SimpleHash(), Hash::Free);
    };

    virtual bool Start() override {
        m_sd = 0x1F665869;
        m_bc = 0;
        for (int i=0; i < (int) sizeof(m_hash); ++i)
            m_hash[i] = 0;
        m_ready = false;
        return true;
    };

    virtual bool Next(const void* ptr, int cch) override {
        if (m_ready)
            return false;
        auto data = reinterpret_cast<const uint8_t*>(ptr);
        uint8_t buf[32];
        while (cch > 0) {
            int bc = (cch >= 32)? 32 : cch;
            Entropy(buf, bc);
            for (int i=0; i < bc; ++i)
                m_hash[i] ^= buf[i] ^ data[i];
            cch -= bc;
        }
        return true;
    };

    virtual string Digest() override {
        string rv;
        m_ready = true; // запрет дальнейших вычислений
        static const char hex[] = "0123456789ABCDEF";
        for (int i=0; i<32; ++i) {
            rv += hex[m_hash[i] >> 0x4];
            rv += hex[m_hash[i] & 0xf];
        }
        return rv;
    };

protected:
    uint32_t m_sd;
    uint32_t m_bc;
    uint8_t m_hash[32];
    bool m_ready;

    void Entropy(uint8_t* to, int n) {
        if (!to || n < 1)
            return;
        uint8_t* const e = reinterpret_cast<uint8_t*>(&m_sd) + 4;
        uint8_t* b = e - m_bc;
        do {
            if (b >= e) {
                m_sd = (m_sd % 127773U) * 16807U - (m_sd / 127773U) * 2836U;
                b = e - 4;
            }
            *to++ = *b++;
        } while (--n);
        m_bc = e - b;
    };

private:
    SimpleHash() {
        Start();
    };
    virtual ~SimpleHash(){
    };
};


struct CommitData
{
    time_t time;
    string hash;
    string author;
    string comment;

    inline bool operator!= (const CommitData& other) {
        return time != other.time || hash != other.hash ||
           author != other.author || comment != other.comment;
    };
};


class Commit
{
public:
    typedef shared_ptr<Commit> Ref;

    /// Создать новый коммит
    static Ref Create(string hash, string author, string comment) {
        Ref empty;
        // TODO: сделать валидацию строки реферала.
        if (author.empty() || author.find(":") != string::npos)
            return empty;
        Ref commit = Ref(new Commit, Commit::Free);
        if (!commit)
            return empty;
        commit->m_commit.hash = hash;
        auto lst = ListPtr(new list<string>());
        if (!lst)
            return empty;
        commit->m_commit.author = author;
        commit->m_commit.comment = comment;
        return commit;
    };

    static Ref Create(const CommitData& data) {
        Ref rv = Ref(new Commit, Commit::Free);
        if (rv)
            rv->m_commit = data;
        return rv;
    };

    inline bool operator!= (const Commit& other) {
        return m_commit != other.m_commit;
    };

    virtual const CommitData& Get() const {
        return m_commit;
    };

    virtual string Build() {
        return to_string(static_cast<unsigned long long>(m_commit.time)) + ":" +
           m_commit.hash + ":" + m_commit.author + ":" + m_commit.comment;
    };

private:
    CommitData m_commit;

    Commit() {
        time(&m_commit.time);
    };
    virtual ~Commit(){
    };
    static void Free(Commit* lpt) {
        delete lpt;
    };
};


class CommitParse: protected SplitString
{
public:
    static Commit::Ref Decompose(string str) {
        Commit::Ref rv;
        CommitParse cp;
        cp.m_stop = FL_NO;
        if (cp.Split(str, ":", BHV::ALLOW) == string::npos)
            return rv;
        return cp.m_stop == FL_NO? Commit::Create(cp.m_data) : rv;
    };

protected:
    CommitData m_data;
    enum FL {
        FL_TIME,
        FL_HASH,
        FL_REFS,
        FL_COMMENT,
        FL_NO
    };
    FL m_stop;

    virtual bool Column(const string& s, size_t n, size_t b, size_t cc) {
        m_stop = static_cast<FL>(n);
        // поля до FL_REFS обязательны!
        if (n < FL_REFS && cc == 0)
            return false;
        switch (m_stop) {
            case FL_TIME: {
                 m_data.time = (time_t) strtoull(s.c_str(), nullptr, 10);
            } break;
            case FL_HASH: {
                m_data.hash = s.substr(b, cc);
            } break;
            case FL_REFS: {
                if (cc == 0)
                    break;
                m_data.author = s.substr(b, cc);
            } break;
            case FL_COMMENT: {
                m_data.comment = s.substr(b);
                m_stop = FL_NO;
            } return false;
            default: return false;
        }
        return true;
    };
};


class TextProc
{
public:
    virtual bool Load(Resource::Ref source) {
        if (!source)
            return false;
        char buf[1024*4]; // наиболее часто используемый размер кластера
        int br;
        while ((br = source->Read(buf, sizeof(buf))) > 0) {
            if (!Next(buf, br))
                return false;
        }
        if (br != 0) // Последняя операция чтения была неудачной
            return false;
        buf[0] = '\n';
        return Next(buf, 1);
    };

protected:
    virtual bool Capture(string str) = 0;

private:
    string m_text;

    virtual bool Next(const char* data, int cc) {
        const char* b = data;
        const char* e = b + cc;
        while (b < e) {
            if (*b == '\n' || *b == '\r') {
                if (data < b) {
                    m_text.append(data, b-data);
                    if (!Capture(m_text))
                        return false;
                    m_text.clear();
                }
                data = ++b; continue;
            }
            ++b;
        }
        return true;
    };
};


class History: public TextProc
{
public:
    typedef shared_ptr<History> Ref;
    typedef list<Commit::Ref> Storage;

    static Ref Create() {
        return Ref(new History(), History::Free);
    };

    virtual const Storage& List() {
        return m_list;
    };

    /// Различия историй коммитов
    struct DIFF {
        Commit::Ref start;  /// Последний общий коммит (если имеется)
        Storage source;     /// Коммиты источника, отсутствующие у цели
        Storage target;     /// Коммиты цели, отсутствующие у источника
    };

    typedef shared_ptr<DIFF> PDIFF;

    /// Сравнивает истории коммитов
    virtual PDIFF Diff(Ref to) {
        PDIFF rv;
        if (!to)
            return rv;
        auto sl = m_list.begin();
        auto se = m_list.end();
        const Storage& tol = to->List();
        auto tl = tol.begin();
        auto te = tol.end();
        rv = std::make_shared<DIFF>();
        if (!rv) // TODO: какой то эксепшн/состояние?
            return rv;
        // пропускаем одинаковые
        for (;;) {
            if (sl == se || tl == te)
                break;
            if (**sl != **tl)
                break;
            rv->start = *sl;
            ++sl; ++tl;
        }
        // Заполняем списки остатками коммитов
        while ( sl != se || tl != te ) {
            if (sl != se) {
                rv->source.push_back(*sl);
                ++sl;
            }
            if (tl != te) {
                rv->target.push_back(*tl);
                ++tl;
            }
        }
        return rv;
    };

    virtual bool Save(Resource::Ref to) {
        if (!to)
            return false;
        if (m_list.empty())
            return false;
        for (auto& it: m_list) {
            string tmp = it->Build() + "\n";
            int cc = tmp.size();
            if (to->Write(tmp.c_str(), cc) != cc)
                return false;
        }
        return to->Flush();
    };

    virtual bool Add(Commit::Ref commit) {
        if (!commit)
            return false;
        const CommitData& cdt = commit->Get();
        if (cdt.hash.empty()) // без хэша добавлять нельзя
            return false;
        if (cdt.author.empty()) // без автора добавлять нельзя
            return false;
        if (!m_list.empty()) {
            // нельзя дважды подряд добавить те же данные (бессмыслено)
            if (m_list.back()->Get().hash == cdt.hash)
                return false;
        }
        m_list.push_back(commit);
        return true;
    };

protected:
    virtual bool Capture(string str) override {
        auto commit = CommitParse::Decompose(str);
        if (!commit)
            return false;
        m_list.push_back(commit);
        return true;
    };

private:
    Storage m_list;

    History() {
    };
    virtual ~History() {
    };
    static void Free(History* lpt) {
        delete lpt;
    };
};


class noncopyable {
protected:
    noncopyable(){}
    ~noncopyable(){}
private:
    noncopyable (const noncopyable&);
    noncopyable& operator=(const noncopyable&);
};


struct Lockable {
    /// блокировка/разблокировка для модификации
    virtual bool Lock(bool lock) = 0;
};


class RepoLock: noncopyable
{
public:
    RepoLock(Lockable* ptr): m_ref(ptr) {
        m_locked = m_ref->Lock(true);
    };

    ~RepoLock() {
        if (m_locked && !UnLock()) {
            // TODO: сделать исключение
        }
    };

    bool UnLock() {
        if (m_locked && m_ref->Lock(false))
            m_locked = false;
        return m_locked == false;
    };

    operator bool() {
        return m_locked;
    };

private:
    Lockable* m_ref;
    bool m_locked;
};


class Repository: protected Lockable
{
public:
    typedef shared_ptr<Repository> Ref;

    typedef Resource::Ref RESOURCE;
    typedef History::Ref  HISTORY;

    typedef Resource::MODE MODE;

    /// "специальные" файлы объекта
    enum SPECIAL {
        SP_META,        /// История объекта
        SP_META_T,      /// Новая история объекта с применением на Move()
        SP_OBJ,         /// Объект из хранилища, obj - хэш объекта
        SP_OBJ_T,       /// Новый объект хранилища с применением на Move()
    };

    /// Возвращает ресурс хранилища для объекта.
    virtual RESOURCE Select(string obj, SPECIAL spec, MODE mode) = 0;

    /// применение специальных временных ресурсов к их назначению
    virtual bool Apply(string obj, SPECIAL spec, RESOURCE res) = 0;

    /// Добавляет новый объект в хранилище и возвращает его хэш при удаче
    virtual Hash::Ref Add(string obj, RESOURCE data) {
        Hash::Ref empty;
        // TODO: сделать валидацию имени объекта
        if (obj.empty())
            return empty;
        // Блокируем хранилище на доступ извне
        RepoLock lock(this);
        if (!lock)
            return empty;
        // ресурс записывается с текущей позиции, поэтому data->Reset() не
        // вызывается, что позволяет отправлять данные даже из мультиплекса.
        // Проверяем доступность ресурса на чтение
        if (data->Mode() != MODE::M_READ)
            return empty;
        // запись нового объекта ведётся во временный файл с расчётом хэш-суммы
        RESOURCE tmp = Select(obj, SP_OBJ_T, MODE::M_TMP);
        if (!tmp)
            return empty;
        // расчёт ведём в отдельном объекте оставляя rv пустым до окончания
        auto hash = SimpleHash::Create();
        if (!hash || !hash->Start())
            return empty;
        uint8_t buf[1024*4]; // наиболее часто используемый размер кластера
        int br;
        while ((br = data->Read(buf, sizeof(buf))) > 0) {
            // сохраняем данные в репозиторий
            if (tmp->Write(buf, br) != br)
                return empty;
            // и одновременно считаем хэш
            if (!hash->Next(buf, br))
                return empty;
        }
        if (br != 0 || !tmp->Flush()) // ошибка чтения/записи
            return empty;
        // по полученному хэшу определяем путь сохранения объекта
        string dgst = hash->Digest();
        if (dgst.empty())
            return empty;
        // проверяем нет ли копии такого объекта в хранилише
        RESOURCE copy = Select(dgst, SP_OBJ, MODE::M_OPERATE);
        if (copy) // если имеется копия, нет нужды сохранять новые данные
            return hash;
        // Применяем временный файл
        if (!Apply(dgst, SP_OBJ, tmp))
            return empty;
        lock.UnLock(); // на случай агрессивной оптимизации...
        return hash;
    };

    /// отправить изменения в удалённый репозиторий, можно даже с перезаписью
    virtual bool Push(string obj, Repository::Ref target, bool overwrite) {
        if (obj.empty() || !target)
            return false;
        // Блокируем хранилища на доступ извне
        RepoLock tlock(this);
        RepoLock rlock(target.get());
        if (!tlock || !rlock)
            return false;
        // TODO: нужно реализовать отправку новой истории и объектов
        auto hist = History(obj);
        if (!hist) // нет локальной истории!
            return false;
        auto target_hist = target->History(obj);
        // если целевая история для объекта недоступна - делаем новую
        if (!target_hist)
            target_hist = History::Create();
        // получаем различия историй коммитов
        auto diff = hist->Diff(target_hist);
        if (!diff) // TODO: уведомление!
            return false;
        // если целевое хранилище имеет больше коммитов...
        if (diff->target.size() > 0) {
            // и у нас имеется ответвление от истории - необходим мерж+push
            if (diff->source.size() > 0) {
                // TODO: реализовать возврат информации о необходимости слияния (merge)
                return false;
            }
            // наш репозиторий просто отстаёт от удалённого хранилища.
            // TODO: Уведомить клиента о необходимости получения коммитов (pull)
            return false;
        }
        // цель не имеет части наших коммитов?
        if (diff->source.size() > 0) {
            // отправляем коммиты
            for (auto& it : diff->source) {
                const CommitData& cdt = it->Get();
                // получаем локальный ресурс коммита
                RESOURCE send = Select(cdt.hash, SP_OBJ, MODE::M_READ);
                // если нет - критическая ошибка (повреждён репозитарий)
                if (!send) // TODO: установить "грязный" флаг репозитария
                    return false;
                // помещаем данные в удалённый репозиторий с проверкой целостности
                auto hash = target->Add(obj, send);
                // проверка совпадения хэшей
                if (!hash || hash->Digest() != cdt.hash) {
                    if (hash) {
                        // TODO: установить "грязный" флаг репозитария
                        // удалять помещёный ресурс нельзя, т.к. он может
                        // пересекаться с другими объектами, если нет - он
                        // будет удалён во время сборки мусора, когда
                        // обнаружится, что ссылок на него больше нет.
                    }
                    return false;
                }
                // добавляем в историю новую запись.
                if (!target_hist->Add(it))
                    return false; // TODO: нужен механизм оповещения!
            }
            // все объекты отправлены, обновляем историю на целевом хранилище
            if (!target->History(obj, target_hist)) {
                // TODO: уведомление!
                return false;
            }
            return true;
        }
        // нечего обновлять (истории идентичны)
        // TODO: оповещение!
        rlock.UnLock();
        tlock.UnLock();
        return true;
    };

    /// Возвращает историю для объекта
    virtual HISTORY History(string obj) {
        RESOURCE res = Select(obj, SP_META, MODE::M_READ);
        HISTORY hist = History::Create();
        if (res && !hist->Load(res)) // если история есть но не загружается
            return HISTORY(nullptr);
        return hist; // если же истории нет для объекта - создаём новую
    };

    /// Запись новой версии объекта в хранилище (или создание)
    virtual bool Update(string obj, string comment, RESOURCE data) {
        if (!data || obj.empty())
            return false;
        // извлечь/создать историю для объекта
        auto hist = History(obj);
        if (!hist)
            return false;
        // добавить данные в хранилище
        auto hash = Add(obj, data);
        if (!hash)
            return false;
        // добавляем новый коммит в историю объекта
        auto commit = Commit::Create(hash->Digest(), m_author, comment);
        if (!hist->Add(commit))
            return false;
        // Обновляем историю объекта в хранилище
        if (!History(obj, hist))
            return false;
        return true;
    };

    /// Извлечение нужной версии
    virtual bool Checkout(string hash, RESOURCE to) {
        if (!to || hash.empty())
            return false;
        if (to->Mode() != MODE::M_WRITE)
            return false;
        RESOURCE from = Select(hash, SP_OBJ, MODE::M_READ);
        if (!from)
            return false;
        auto shs = SimpleHash::Create();
        if (!shs || !shs->Start())
            return false;
        uint8_t buf[1024*4]; // наиболее часто используемый размер кластера
        int br;
        while ((br = from->Read(buf, sizeof(buf))) > 0) {
            // сохраняем данные в репозиторий
            if (to->Write(buf, br) != br)
                return false;
            // и одновременно считаем хэш
            if (!shs->Next(buf, br))
                return false;
        }
        if (br != 0 || !to->Flush()) // ошибка чтения/записи...
            return false;
        return shs->Digest() == hash;
    };

protected:
    const string m_author;

    /// Запись новой истории объекта
    virtual bool History(string obj, HISTORY hist) {
        // сохраняем во временный файл истории
        RESOURCE tmp = Select(obj, SP_META_T, MODE::M_TMP);
        // если не получилось сохранить историю объекта
        if (!tmp || !hist->Save(tmp))
            return false;
        // применяем временный файл
        return Apply(obj, SP_META, tmp);
    };

    static void Free(Repository* lpt) {
        delete lpt;
    };
    Repository(string author): m_author(author) {
    };
    virtual ~Repository() {
    };
};


class File: public Resource
{
public:
    static Ref Create(string path, Resource::MODE mode) {
        Ref rv(new File(path, mode), Resource::Free);
        if (rv && rv->Mode() == M_ERROR)
            rv.reset();
        return rv;
    };

protected:
    virtual int Read(void* buf, int cb) override {
        if (m_ref == NULL || cb < 1)
            return false;
        if (m_mode != M_READ)
            return false;
        return ::fread(buf, 1, cb, m_ref);
    };

    virtual int Write(const void* buf, int cb) override {
        if (m_ref == NULL || cb < 1)
            return false;
        if (m_mode != M_TMP && m_mode != M_WRITE)
            return false;
        return ::fwrite(buf, 1, cb, m_ref);
    };

    virtual bool Move(string uri) override {
        if (m_ref) {
            fclose(m_ref);
            m_ref = NULL;
            if (m_mode != M_TMP) // из M_TMP выходим только по удачному Move()
                m_mode = M_OPERATE;
        }
        if (m_mode != M_OPERATE && m_mode != M_TMP)
            return false;
        if (m_uri == uri)
            return false;
        if (fs::move(m_uri, uri)) {
            m_mode = M_OPERATE;
            return true;
        }
        return false;
    };

    virtual bool Remove() override {
        if (m_ref) {
            fclose(m_ref);
            m_ref = NULL;
            m_mode = M_OPERATE;
        }
         if (m_mode != M_OPERATE)
            return false;
        return fs::remove(m_uri);
    };

    virtual bool Reset() override {
        switch (m_mode) {
            case M_TMP:
            case M_READ:
            case M_WRITE:
                break;
            default: return false;
        }
        return ::fseek(m_ref, SEEK_SET, 0) == 0;
    };

    virtual bool Flush() override {
        switch (m_mode) {
            case M_TMP:
            case M_WRITE:
                break;
            default: return false;
        }
        return ::fflush(m_ref) == 0;
    };

private:
    FILE* m_ref;
    string m_uri;

    File(string path, Resource::MODE mode):
        m_ref(NULL), m_uri(path)
    {
        FILE* exist = ::fopen(m_uri.c_str(), "rb");
        if (mode == M_READ) {
            if (exist) {
                m_mode = M_READ;
                m_ref = exist; // переиспользуем
            }
            return;
        }
        if (exist)
            ::fclose(exist);

        if (mode == M_OPERATE) {
            if (exist)
                m_mode = M_OPERATE;
        }
        else if (mode == M_TMP) {
            if (exist == NULL) {
                m_ref = ::fopen(m_uri.c_str(), "wb");
                if (m_ref)
                    m_mode = M_TMP;
            }
        }
        else if (mode == M_WRITE) {
            if (exist && remove(m_uri.c_str()) != 0)
                return;
            m_ref = fopen(m_uri.c_str(), "wb");
            if (m_ref)
                m_mode = M_WRITE;
        }
    };

    virtual ~File() {
        if (m_ref) {
            fclose(m_ref);
            m_ref = NULL;
            m_mode = M_ERROR;
        }
    };

    struct fs {
        static bool remove(string path) {
            return ::remove(path.c_str()) == 0;
        };

        static bool move(string from, string to) {
            mkdir(parent(to));
            return ::rename(from.c_str(), to.c_str()) == 0;
        };

        static string parent(string path) {
            size_t cp = path.find_last_of("\\/");
            if (cp && cp != string::npos)
                return path.substr(0, cp);
            return string();
        };

        static bool mkdir(string path) {
            if (path.empty())
                return false;
            size_t cp = 0;
            if (path.size() > 3 && path[1] == ':')
                cp = 3;
            while ((cp = path.find_first_of("\\/", cp + 1)) != string::npos)
                ::_mkdir(path.substr(0, cp).c_str());
            return ::_mkdir(path.c_str()) == 0;
        };
    };
};


class RepoLocal: public Repository
{
public:
    static Repository::Ref Create(string dir, string suid, bool compress) {
        return Ref(new RepoLocal(dir, suid, compress), Repository::Free);
    };

protected:
    bool Lock(bool lock) override {
        return true;
    };

    /// Возвращает ресурс хранилища для объекта.
    virtual RESOURCE Select(string obj, SPECIAL spec, MODE mode) {
        cout << "select: ";
        RESOURCE rv;
        string path;
        switch (spec) {
            case SP_META:   path = obj + ".doss"; break;
            case SP_META_T: path = obj + ".dos" + "~"; break;
            case SP_OBJ:    path = "obj/" + obj.substr(0,2) + "/" + obj.substr(2); break;
            case SP_OBJ_T:  path = obj + ".tmp"; break;
        }
        if (path.empty())
            return rv;
        path = m_dir + "/" + path;
        cout << "[" << path << "]";
        if (m_compress)
            rv = GZResource::Create(File::Create(path, mode));
        else
            rv = File::Create(path, mode);
        cout << (rv?" - ok":"") << endl;
        return rv;
    };

    /// применение специальных временных ресурсов к их назначению
    virtual bool Apply(string obj, SPECIAL spec, RESOURCE res) {
        if (!res)
            return false;
        string path;
        switch (spec) {
            case SP_META:   path = obj + ".doss"; break;
            case SP_OBJ:    path = "obj/" + obj.substr(0,2) + "/" + obj.substr(2); break;
            default: break;
        }
        if (path.empty())
             return false;
        path = m_dir + "/" + path;
        // заменяемый ресурс (если существовал)
        RESOURCE old;
        // если меняем историю
        if (spec == SP_META) {
            // то удаляем бэкап файла истории (если есть)
            auto bc = File::Create(path + ".bak", MODE::M_OPERATE);
            if (bc && !bc->Remove()) // если не удалось удалить бэкап
                return false;
            // Сохраняем бэкап истории объекта (если была)
            bc = File::Create(path, MODE::M_OPERATE);
            // если история была, но переместить неудалось...
            if (bc && !bc->Move(path + ".bak"))
                return false;
            old = bc; // для отмены перемещения
        }
        // для остальных объектов не требуется бэкапов
        if (!res->Move(path)) {
            if (old && !old->Move(path)) {
                // TODO: заблокировать репозиторий для дальнейшей работы!
            }
            return false;
        }
        return true;
    };

private:
    string m_dir;
    const bool m_compress;

    RepoLocal(string dir, string suid, bool compress):
        Repository(suid), m_compress(compress)
    {
        if (dir.empty())
            return;
        m_dir = dir;
        int lc = m_dir.size() - 1;
        char ch = m_dir[lc];
        if (ch == '/' || ch == '\\')
            m_dir.resize(lc);
    };
    virtual ~RepoLocal() {
    };
};


int main(int argc, char** argv)
{
    // author должен быть уникальным для каждой точки синхронизации
    auto repo = RepoLocal::Create("k:/PROJECTS/OPEN/doss", "home-comp", false);
    if (!repo)
        return -1;
    auto res = File::Create("doss.cpp", Resource::M_READ);
    cout << "Update: test-obj" << endl;
    if (repo->Update("test-obj", "* initial", res)) {
        cout << "ok!" << endl;
    }
    cout << "Get history:" << endl;
    auto hist = repo->History("test-obj");
    if (hist) {
        cout << "ok!" << endl;
        cout << "Get commit:" << endl;
        Commit::Ref commit = hist->List().front(); // первый коммит
        if (commit) {
            cout << "ok!" << endl;
            auto inf = commit->Get();
            cout << "get commit: " << inf.hash << endl;
            auto tmp = File::Create("doss.get", Resource::M_WRITE);
            cout << "Checkout:" << endl;
            if (repo->Checkout(inf.hash, tmp)) {
                cout << "ok!" << endl;
            }
        }
    }
    auto repo2 = RepoLocal::Create("C:/tmp/repo", "other-comp", true);
    if (!repo2)
        return -1;
    if (repo->Push("test-obj", repo2, false))
        cout << "Push - ok" << endl;
    return 0;
}
